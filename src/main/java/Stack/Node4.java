package Stack;

public class Node4 {
    public Node4 next;
    public String data;

    public Node4(String data){
        this.data = data;
    }

    public void setNext(Node4 next) {
        this.next = next;
    }

    public String toString(){
        return this.data;
    }
}

