package Stack;


public class Stack {
    public Node4 top;
    int i = 0;

    public Stack() {

    }

    public Node4 top() {
        return top;
    }

    public void push(Node4 node) {
        Node4 temp = this.top;
        this.top = node;
        node.next = temp;
        i++;
    }

    public int size() {
        return i;
    }

    public boolean isEmpty() {
        if (top == null) {
            return true;
        } else {
            return false;
        }
    }

    public Node4 pop() {
        if (i != 0) {
            Node4 term = top;
            top = top.next;
            i = i - 1;
            return term;
        } else {
            Node4 term = null;
            i = i - 1;
            return term;
        }
    }


}
