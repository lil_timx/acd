package kz.aitu.week1;

import java.util.Date;

public class BigOcs03 {

    //O(1)
    public static void func1(int n) {
        System.out.println("Hey - your input is: " + n);

        System.out.println("Hey - your input is: " + n);
        System.out.println("Hmm.. I'm doing more stuff with: " + n);
        System.out.println("And more: " + n);
    }

    //O(n)
    public static void func2(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println("Hey - I'm busy looking at: " + i);
            System.out.println("Hmm.. Let's have another look at: " + i);
            System.out.println("And another: " + i);
        }
    }


    //O(log(n))
    public static void func3(int n) {
        for (int i = 1; i < n; i = i * 2){
            System.out.println("Hey - I'm busy looking at: " + i);
        }
    }

    //O(1)
    public static void func5(int n) {
        for (int i = 1; i <= 8; i++) {
            System.out.println("Hey - I'm busy looking at: " + i);
        }
    }

    //O(n^2)
    public static void func6(int n) {
        for (int i = 1; i <= n; i++) {
            for(int j = 1; j <= n; j++) {
                System.out.println("Hey - I'm busy looking at: " + i + " and " + j);
            }
        }
    }

    //O(n*4^n)
    public static void func7(int n) {
        int c = 1;
        for (int b = 1; b <= 100; b++) {
            for (int a = 1; a <= n; a++) {
                for (int i = 1; i <= Math.pow(2, n); i++) {
                    for (int j = 1; j <= Math.pow(2, n); j++) {
                        System.out.println("Hey - I'm busy looking at: " + c++);
                    }
                }
            }
        }
        for (int b = 1; b <= 100; b++) {
            for (int a = 1; a <= n; a++) {
                for (int i = 1; i <= Math.pow(2, n); i++) {
                    for (int j = 1; j <= Math.pow(2, n); j++) {
                        System.out.println("Hey - I'm busy looking at: " + c++);
                    }
                }
            }
        }
    }

    public static boolean func10(int n) {
        String s = String.valueOf(n);
        int k = 0;
        for(int i = 0; i < s.length() / 2; i++) {
            k++;
            System.out.println(i + " <-> " + (s.length() - 1 - i));
            if(s.charAt(i) != s.charAt(s.length() - 1 - i)) return false;
        }
        System.out.println(k);
        return true;
    }

    public static void main(String[] args) {
        int n = 4242424;
        System.out.println(func10(n));
    }
}
