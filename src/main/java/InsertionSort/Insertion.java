package InsertionSort;


public class Insertion {
    void sort(int[] arr) {
        for (int i = 1; i < arr.length; ++i) {
            int key = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }



    void printArray(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    public static void main(String args[]) {
        Insertion ob = new Insertion();
        int arr[] = {64, 34, 25, 12, 22, 22, 90};
        ob.sort(arr);
        System.out.println("Sorted array");
        ob.printArray(arr);
    }
}






