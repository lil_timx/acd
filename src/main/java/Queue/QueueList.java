package Queue;



public class QueueList{
    QueueNode head;
    QueueNode tail;
    int i=0;


    public QueueNode head() {
        return head;
    }

    public QueueNode tail() {
        return tail;
    }
    public void push(String data){
        QueueNode node = new QueueNode(data);
        if(head==null){
            head=node;
            tail=node;
        }
        else{
            tail.next=node;
            tail=node;
        }
        i++;
    }
    public QueueNode peek(){
        return head;
    }
    public QueueNode poll(){
        QueueNode acd=head;
        head = head.next;
        i=i-1;
        return acd;
    }
    public int size(){
        return i;
    }
    public QueueNode pop(){
        QueueNode temp = tail;
        QueueNode acd = head;
        while(acd.next.next != null){
            acd=acd.next;

        }
        i=i-1;
        tail=acd;
        tail.next = null;
        return temp;
    }
    public boolean empty(){
        if(head==null){
            return true;
        }
        else{
            return false;
        }
    }
}


