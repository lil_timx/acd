package Queue;




    public class QueueNode {
        public QueueNode next;
        public String data;

        public QueueNode(String data){
            this.data = data;
        }


        public void setNext(QueueNode next) {
            this.next = next;
        }

        public String toString(){
            return this.data;
        }
    }



