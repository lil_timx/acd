package Queue;

public class QueueTest {
    public static void main(String[] args) {
        Queue.QueueList firstQ = new QueueList();

        firstQ.push("Iphone");
        firstQ.push("Samsung");
        firstQ.push("Nokia");
        firstQ.push("Xiomi");
        firstQ.push("Huwawei");
        System.out.println(firstQ.size());
        System.out.println(firstQ.poll());
        System.out.println(firstQ.peek());
        System.out.println(firstQ.poll());
        System.out.println(firstQ.poll());
        System.out.println(firstQ.size());
        System.out.println(firstQ.empty());


    }
}
